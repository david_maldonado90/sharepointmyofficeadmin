declare interface IAdminWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'AdminWebPartStrings' {
  const strings: IAdminWebPartStrings;
  export = strings;
}
